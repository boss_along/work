package com.example.work.controller;

import com.example.work.common.model.PageModel;
import com.example.work.common.model.Result;
import com.example.work.common.utils.LoginUserUtils;
import com.example.work.entity.TbUserDTO;
import com.example.work.entity.TbWorkCountDTO;
import com.example.work.entity.TbWorkDTO;
import com.example.work.service.TbUserService;
import com.example.work.service.TbWorkCountService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (TbWorkCount)表控制层
 *
 * @author makejava
 * @since 2019-02-16 07:39:59
 */
@Controller
@RequestMapping("tbWorkCount")
public class TbWorkCountController {
    /**
     * 服务对象
     */
    @Resource
    private TbWorkCountService tbWorkCountService;
    @Autowired
    private TbUserService tbUserService;


    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public TbWorkCountDTO selectOne(String id) {
        return this.tbWorkCountService.queryById(id);
    }




    /**
     * 跳转到考勤列表页面
     * @return
     */
    @RequestMapping("index")
    public String index(String page){
        if("前台".equals(page)){
            return "index/workcount/list";
        }
        if("后台".equals(page)){
            return "back/workcount/list";
        }
        return null;
    }

    /**
     * 考勤页面表格
     * @return
     */
    @RequestMapping("list")
    @ResponseBody
    public PageModel list(TbWorkCountDTO tbWorkCountDTO,@RequestParam(value = "page", defaultValue = "0") Integer page,@RequestParam(value = "limit", defaultValue = "10")Integer limit){
        PageHelper.startPage(page,limit);
        List<TbWorkCountDTO> list = tbWorkCountService.queryAllByLimit(tbWorkCountDTO);
        PageInfo pageInfo = new PageInfo(list);
        PageModel pageModel = new PageModel(pageInfo.getTotal(), list);
        return pageModel;
    }

    /**
     * 跳转到新增页面
     * @param map
     * @return
     */
    @RequestMapping("/toadd")
    public String toadd(ModelMap map){
        TbUserDTO tbUserDTO = new TbUserDTO();
        tbUserDTO.setRole("员工");
        List<TbUserDTO> list = tbUserService.queryAll(tbUserDTO);
        map.put("list",list);
        return "back/workcount/add";
    }

    /**
     * 新增
     * @param tbWorkCountDTO
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public Result add( TbWorkCountDTO tbWorkCountDTO){
        tbWorkCountService.insert(tbWorkCountDTO);
        return Result.ok();
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/del")
    @ResponseBody
    public Result del(String id){
        tbWorkCountService.deleteById(id);
        return Result.ok();
    }

    /**
     * 修改页面
     * @param id
     * @param modelMap
     * @return
     */
    @GetMapping("toupdate/{id}")
    public String toupdate(@PathVariable("id") String id, ModelMap modelMap) {

        TbWorkCountDTO tbWorkCountDTO = tbWorkCountService.queryById(id);
        modelMap.put("workcount",tbWorkCountDTO);
        return "back/workcount/update";
    }

    /**
     * 修改操作
     * @param tbWorkCountDTO
     * @return
     */
    @RequestMapping("/update")
    @ResponseBody
    public Result update(TbWorkCountDTO tbWorkCountDTO){
        tbWorkCountService.update(tbWorkCountDTO);
        return Result.ok();
    }
}