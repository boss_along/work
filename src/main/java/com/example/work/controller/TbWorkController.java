package com.example.work.controller;

import com.example.work.common.model.PageModel;
import com.example.work.common.model.Result;
import com.example.work.common.utils.LoginUserUtils;
import com.example.work.entity.TbUserDTO;
import com.example.work.entity.TbWorkDTO;
import com.example.work.service.TbUserService;
import com.example.work.service.TbWorkService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * (TbWork)表控制层
 *
 * @author makejava
 * @since 2019-02-16 07:39:56
 */
@Controller
@RequestMapping("tbWork")
public class TbWorkController {
    /**
     * 服务对象
     */
    @Resource
    private TbWorkService tbWorkService;

    @Autowired
    private TbUserService tbUserService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public TbWorkDTO selectOne(String id) {
        return this.tbWorkService.queryById(id);
    }

    /**
     * 跳转到考勤列表页面
     * @return
     */
    @RequestMapping("index")
    public String index(String page){
        if("前台".equals(page)){
            return "index/work/list";
        }
        if("后台".equals(page)){
            return "back/work/list";
        }
        return null;
    }

    /**
     * 考勤页面表格
     * @return
     */
    @RequestMapping("list")
    @ResponseBody
    public PageModel list(){
        String id = LoginUserUtils.getUserInfo().getId();
        List<TbWorkDTO> list = tbWorkService.queryByUserId(id);
        PageInfo pageInfo = new PageInfo(list);
        PageModel pageModel = new PageModel(pageInfo.getTotal(), list);
        return pageModel;
    }

    /**
     * 跳转到新增页面
     * @param map
     * @return
     */
    @RequestMapping("/toadd")
    public String toadd(ModelMap map){
        TbUserDTO tbUserDTO = new TbUserDTO();
        tbUserDTO.setRole("员工");
        List<TbUserDTO> list = tbUserService.queryAll(tbUserDTO);
        map.put("list",list);
        return "back/work/add";
    }

    /**
     * 新增
     * @param tbWorkDTO
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public Result add(TbWorkDTO tbWorkDTO){
        tbWorkService.insert(tbWorkDTO);
        return Result.ok();
    }

    /**
     * 管理员新增
     * @param tbWorkDTO
     * @return
     */
    @RequestMapping("/managerAdd")
    @ResponseBody
    public Result managerAdd(TbWorkDTO tbWorkDTO){
        tbWorkService.managerInsert(tbWorkDTO);
        return Result.ok();
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/del")
    @ResponseBody
    public Result del(String id){
        tbWorkService.deleteById(id);
        return Result.ok();
    }

    /**
     * 修改页面
     * @param id
     * @param modelMap
     * @return
     */
    @GetMapping("toupdate/{id}")
    public String toupdate(@PathVariable("id") String id, ModelMap modelMap) {

        TbWorkDTO tbWorkDTO = tbWorkService.queryById(id);
        String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tbWorkDTO.getCardTime());
        tbWorkDTO.setPattenCardTime(format);
        modelMap.put("work",tbWorkDTO);
        return "back/work/update";
    }

    /**
     * 修改操作
     * @param tbWorkDTO
     * @return
     */
    @RequestMapping("/update")
    @ResponseBody
    public Result update(TbWorkDTO tbWorkDTO){
        tbWorkService.update(tbWorkDTO);
        return Result.ok();
    }
}