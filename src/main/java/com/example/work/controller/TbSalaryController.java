package com.example.work.controller;

import com.example.work.common.model.PageModel;
import com.example.work.common.model.Result;
import com.example.work.entity.TbSalaryDTO;
import com.example.work.entity.TbUserDTO;
import com.example.work.entity.TbWorkCountDTO;
import com.example.work.service.TbSalaryService;
import com.example.work.service.TbUserService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * (TbSalary)表控制层
 *
 * @author makejava
 * @since 2019-02-16 07:39:53
 */
@Controller
@RequestMapping("tbSalary")
public class TbSalaryController {
    /**
     * 服务对象
     */
    @Resource
    private TbSalaryService tbSalaryService;
    @Autowired
    private TbUserService tbUserService;


    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public TbSalaryDTO selectOne(String id) {
        return this.tbSalaryService.queryById(id);
    }


    /**
     * 跳转到考勤列表页面
     * @return
     */
    @RequestMapping("index")
    public String index(String page){
        if("前台".equals(page)){
            return "index/salary/list";
        }
        if("后台".equals(page)){
            return "back/salary/list";
        }
        return null;
    }

    /**
     * 考勤页面表格
     * @return
     */
    @RequestMapping("list")
    @ResponseBody
    public PageModel list(TbSalaryDTO tbSalary){
        List<TbSalaryDTO> list = tbSalaryService.queryAllByLimit(tbSalary);
        PageInfo pageInfo = new PageInfo(list);
        PageModel pageModel = new PageModel(pageInfo.getTotal(), list);
        return pageModel;
    }

    /**
     * 跳转到新增页面
     * @param map
     * @return
     */
    @RequestMapping("/toadd")
    public String toadd(ModelMap map){
        TbUserDTO tbUserDTO = new TbUserDTO();
        tbUserDTO.setRole("员工");
        List<TbUserDTO> list = tbUserService.queryAll(tbUserDTO);
        map.put("list",list);
        return "back/salary/add";
    }

    /**
     * 新增
     * @param tbSalary
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public Result add(TbSalaryDTO tbSalary){
        tbSalaryService.insert(tbSalary);
        return Result.ok();
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/del")
    @ResponseBody
    public Result del(String id){
        tbSalaryService.deleteById(id);
        return Result.ok();
    }

    /**
     * 修改页面
     * @param id
     * @param modelMap
     * @return
     */
    @GetMapping("toupdate/{id}")
    public String toupdate(@PathVariable("id") String id, ModelMap modelMap) {

        TbSalaryDTO tbSalaryDTO = tbSalaryService.queryById(id);
        String format = new SimpleDateFormat("yyyy-MM").format(tbSalaryDTO.getMonth());
        tbSalaryDTO.setPattenMonth(format);
        modelMap.put("salary",tbSalaryDTO);
        return "back/salary/update";
    }

    /**
     * 修改操作
     * @param tbSalary
     * @return
     */
    @RequestMapping("/update")
    @ResponseBody
    public Result update(TbSalaryDTO tbSalary){
        tbSalaryService.update(tbSalary);
        return Result.ok();
    }
}