package com.example.work.controller;

import com.example.work.entity.TbTestDTO;
import com.example.work.service.TbTestService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * (TbTest)表控制层
 *
 * @author makejava
 * @since 2019-03-21 09:51:43
 */
@Controller
@RequestMapping("tbTest")
public class TbTestController {
    /**
     * 服务对象
     */
    @Autowired
    private TbTestService tbTestService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public TbTestDTO selectOne(Integer id) {
        return this.tbTestService.queryById(id);
    }

}