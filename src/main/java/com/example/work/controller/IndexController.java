package com.example.work.controller;

import com.example.work.common.exception.BusinessException;
import com.example.work.common.model.ErrorPosition;
import com.example.work.common.utils.ExcelValidUtils;
import com.example.work.common.utils.ExcelValidateWarpper;
import com.example.work.entity.TbTestDTO;
import com.example.work.entity.TbUserDTO;
import com.example.work.service.TbUserService;
import lombok.extern.slf4j.Slf4j;
import net.sf.saxon.trans.Err;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * @author Thinkpad
 * @Title: IndexController
 * @ProjectName work
 * @Description: TODO
 * @date 2019/2/1411:25
 * @Version: 1.0
 */
@Controller
@Slf4j
public class IndexController {

    @Autowired
    private TbUserService tbUserService;
    /**
     * 登陆页面
     * @param modelMap
     * @return
     */
    @RequestMapping("/")
    public String index(ModelMap modelMap){
        return "index";
    }

    /**
     * 登陆后的主页面
     * @param modelMap
     * @return
     */
    @RequestMapping("/index")
    public String mian(ModelMap modelMap,String role){

        if("员工".equals(role)){
            return "index/index";
        }
        if("管理员".equals(role)){
            return "back/index";
        }
        return null;
    }

    /*@Autowired
    private RedissonClient redissonClient;

    @RequestMapping("/lock/{id}")
    public Object get(@PathVariable("id") String id){

        ExecutorService executorService = Executors.newFixedThreadPool(10);
        for(int i=0 ;i<1000;i++){
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    RLock lock = null;
                    String name = Thread.currentThread().getName();
                    try {
                        Thread.sleep(2000);
                        lock = redissonClient.getLock(id);
                        boolean b = lock.tryLock(5, 6, TimeUnit.SECONDS);
                        if(b){
                            System.out.println("线程"+name+"获得锁");
                            Thread.sleep(4000);
                        }else {
                            System.out.println("锁被被占用");
                        }

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }finally {
                        lock.unlock();
                    }
                }
            });
        }
        return new Object();



    }*/

    @RequestMapping("/a")
    @ResponseBody
    public String a(ModelMap modelMap){
        List<TbUserDTO> tbUserDTOS = tbUserService.queryListById("1");
        //tbUserService.update(tbUserDTOS.get(0));
        return "index";
    }

    @RequestMapping("/import")
    @ResponseBody
    public List<ErrorPosition> login(@RequestParam(value = "files",required = true) MultipartFile[] files){


            Workbook workbook = ExcelValidUtils.createWorkbook(files[0]);

            String standardTitles[] = {"a","b","c","d","e"};

            ExcelValidateWarpper excelValidateWarpper = new ExcelValidateWarpper(workbook, standardTitles).validSheet().validDataCount().validTitle();

            List<ErrorPosition> list = excelValidateWarpper.getSortedErrInfo();

            return list;

    }


    @RequestMapping("/b")
    @ResponseBody
    public TbTestDTO b(@RequestBody TbTestDTO dto){

        return dto;
    }


}
