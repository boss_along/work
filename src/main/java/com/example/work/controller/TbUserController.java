package com.example.work.controller;

import com.example.work.common.model.PageModel;
import com.example.work.common.model.Result;
import com.example.work.entity.TbUserDTO;
import com.example.work.service.TbUserService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Objects;

/**
 * (TbUser)表控制层
 *
 * @author makejava
 * @since 2019-02-16 07:39:54
 */
@Controller
@RequestMapping("tbUser")
public class TbUserController {
    /**
     * 服务对象
     */
    @Resource
    private TbUserService tbUserService;

    /**
     *
     * @param id
     * @param modelMap
     * @return
     */
    @RequestMapping("index")
    public String index(String page, ModelMap modelMap) {
        if("前台".equals(page)){
            return "index/user/list";
        }
        if("后台".equals(page)){
            return "back/user/list";
        }
        return null;
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne/{id}")
    @ResponseBody
    public PageModel selectOne(@PathVariable(value="id") String id, ModelMap modelMap) {

        PageHelper.startPage(1, 1);
        List<TbUserDTO> list = null;
        if("-1".equals(id)){
            list = tbUserService.queryAll(new TbUserDTO());
        }else{
            list = tbUserService.queryListById(id);
        }

        PageInfo pageInfo = new PageInfo(list);
        PageModel pageModel = new PageModel(pageInfo.getTotal(), list);
        return pageModel;
    }

    @GetMapping("toupdate/{id}")
    public String toupdate(@PathVariable("id") String id, ModelMap modelMap) {

        TbUserDTO tbUserDTO = tbUserService.queryById(id);
        modelMap.put("user",tbUserDTO);
        return "index/user/update";
    }

    @RequestMapping("/login")
    @ResponseBody
    public Result selectOne(String username, String password,String role, String regx, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Object sourceRegx = session.getAttribute("regx");

        /*if(StringUtils.isEmpty(regx)||!regx.equals(sourceRegx)){
            return Result.error(200,"验证码错误");
        }*/

        TbUserDTO tbUserDTO = tbUserService.queryByUsername(username, password,role);
        if(Objects.isNull(tbUserDTO)){
            return Result.error(200,"用户名或密码错误");
        }
        session.setAttribute("loginUser",tbUserDTO);
        return Result.ok(tbUserDTO);

    }

    /**
     * 修改用户信息
     * @param tbUserDTO
     * @return
     */
    @RequestMapping("/update")
    @ResponseBody
    public Result update(TbUserDTO tbUserDTO){
        tbUserService.update(tbUserDTO);
        return Result.ok();
    }


    /**
     * 跳转到新增页面
     * @param tbUserDTO
     * @return
     */
    @RequestMapping("/toadd")
    public String toadd(TbUserDTO tbUserDTO){
        //tbUserService.queryById()
        return "back/user/add";
    }

    /**
     * 新增
     * @param tbUserDTO
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public Result add(TbUserDTO tbUserDTO){
        tbUserService.insert(tbUserDTO);
        return Result.ok();
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/del")
    @ResponseBody
    public Result del(String id){
        tbUserService.deleteById(id);
        return Result.ok();
    }
}