package com.example.work.entity;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * (TbTest)实体类
 *
 * @author makejava
 * @since 2019-03-21 09:51:43
 */
@Data
public class TbTestDTO implements Serializable {
    
    private static final long serialVersionUID = 779400414329757134L;    
            
    /** */
    private Integer id;

    /** */
    private String name;

    /** */
    private Integer age;

    /** */
    private Integer version;

    private List<TbUserDTO> list;
        
}