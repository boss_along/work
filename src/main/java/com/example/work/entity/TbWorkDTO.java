package com.example.work.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * (TbWork)实体类
 *
 * @author makejava
 * @since 2019-03-04 16:35:38
 */
@Data
public class TbWorkDTO implements Serializable {
    
    private static final long serialVersionUID = 930219763876463482L;    
            
    /**
    * ID    
    */    
    private String id;   
            
    /**
    * 用户ID    
    */    
    private String userId;   
            
    /**
    * 打卡时间    
    */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date cardTime;

    /**
     * 格式化大开间
     */
    private String pattenCardTime;
            
    /**
    * 打卡类别
            0-上班卡
            1-下班卡    
    */    
    private String type;   
            
    /**
    * 是否迟到    
    */    
    private String isArriveLater;   
            
    /**
    * 是否早退    
    */    
    private String isLeaveEarly;   
            
    /**
    * 打卡状态
            0：未打卡
            1：打卡
            2：    
    */    
    private String workStatus;   
            
    /**
    * 创建人    
    */    
    private String creater;   
            
    /**
    * 创建时间    
    */    
    private Date createTime;   
            
    /**
    * 修改人    
    */    
    private String modifier;   
            
    /**
    * 修改时间    
    */    
    private Date modifyTime;   
            
    /**
    * 状态    
    */    
    private String status;   
        
}