package com.example.work.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * (TbWorkCount)实体类
 *
 * @author makejava
 * @since 2019-03-04 16:35:38
 */
@Data
public class TbWorkCountDTO implements Serializable {
    
    private static final long serialVersionUID = 127372730618684106L;    
            
    /**
    * ID    
    */    
    private String id;   
            
    /**
    * 用户ID    
    */    
    private String userId;   
            
    /**
    * 考勤年份    
    */    
    private String year;   
            
    /**
    * 考勤月份    
    */    
    private String month;   
            
    /**
    * 出勤天数    
    */    
    private Integer workDay;   
            
    /**
    * 请假天数    
    */    
    private Integer restDay;   
            
    /**
    * 迟到天数    
    */    
    private Integer arriveLateDay;   
            
    /**
    * 早退天数    
    */    
    private Integer leaveEarly;   
            
    /**
    * 创建人    
    */    
    private String creater;   
            
    /**
    * 创建时间    
    */    
    private Date createTime;   
            
    /**
    * 修改人    
    */    
    private String modifier;   
            
    /**
    * 修改时间    
    */    
    private Date modifyTime;   
            
    /**
    * 状态    
    */    
    private String status;   
        
}