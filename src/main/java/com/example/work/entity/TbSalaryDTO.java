package com.example.work.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * (TbSalary)实体类
 *
 * @author makejava
 * @since 2019-03-04 16:35:37
 */
@Data
public class TbSalaryDTO implements Serializable {
    
    private static final long serialVersionUID = 780904034488114734L;    
            
    /**
    * ID    
    */    
    private String id;   
            
    /**
    * 用户ID    
    */    
    private String userId;   
            
    /**
    * 薪资月份    
    */
    @JsonFormat(pattern="yyyy-MM")
    @DateTimeFormat(pattern="yyyy-MM")
    private Date month;

    private String pattenMonth;
            
    /**
    * 总额    
    */    
    private Double amt;   
            
    /**
    * 创建人    
    */    
    private String creater;   
            
    /**
    * 创建时间    
    */    
    private Date createTime;   
            
    /**
    * 修改人    
    */    
    private String modifier;   
            
    /**
    * 修改时间    
    */    
    private Date modifyTime;   
            
    /**
    * 状态    
    */    
    private String status;   
        
}