package com.example.work.dao;

import com.example.work.entity.TbUserDTO;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (TbUser)表数据库访问层
 *
 * @author makejava
 * @since 2019-03-04 15:03:19
 */
public interface TbUserDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TbUserDTO queryById(String id);
    
    /**
     * 条件查询数据
     * @param tbUser 参数
     * @return 实例对象
     */    
    TbUserDTO queryByParam(TbUserDTO tbUser);

    /**
     * 通过用户名查询
     * @param username
     * @return
     */
    TbUserDTO queryByUserName(@Param("username") String username);


    /**
     * 主键查询
     * @param id
     * @return
     */
    List<TbUserDTO> queryListById(@Param("id")String id);
    /**
     * 查询指定行数据
     *
     * @param tbUser 查询起始位置
     * @return 对象列表
     */
    List<TbUserDTO> queryAllByLimit(TbUserDTO tbUser);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param tbUser 实例对象
     * @return 对象列表
     */
    List<TbUserDTO> queryAll(TbUserDTO tbUser);

    /**
     * 新增数据
     *
     * @param tbUser 实例对象
     * @return 影响行数
     */
    int insert(TbUserDTO tbUser);

    /**
     * 修改数据
     *
     * @param tbUser 实例对象
     * @return 影响行数
     */
    int update(TbUserDTO tbUser);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

}