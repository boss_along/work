package com.example.work.dao;

import com.example.work.entity.TbTestDTO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * (TbTest)表数据库访问层
 *
 * @author makejava
 * @since 2019-03-21 09:51:43
 */
public interface TbTestDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TbTestDTO queryById(Integer id);
    
    /**
     * 条件查询数据
     * @param tbTest 参数
     * @return 实例对象
     */    
    TbTestDTO queryByParam(TbTestDTO tbTest);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<TbTestDTO> queryAllByLimit(TbTestDTO tbTest);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param tbTest 实例对象
     * @return 对象列表
     */
    List<TbTestDTO> queryAll(TbTestDTO tbTest);

    /**
     * 新增数据
     *
     * @param tbTest 实例对象
     * @return 影响行数
     */
    int insert(TbTestDTO tbTest);

    /**
     * 修改数据
     *
     * @param tbTest 实例对象
     * @return 影响行数
     *//*
    int update(TbTestDTO tbTest);*/

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

    @Update(" update ${taName} set version = version+1 where id=#{id} and version=#{version} and ${condition}")
    int update(@Param("taName") String taName,@Param("id") String id,@Param("version") String version,@Param("condition") String condition);

}