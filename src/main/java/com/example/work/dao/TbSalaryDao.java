package com.example.work.dao;

import com.example.work.entity.TbSalaryDTO;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (TbSalary)表数据库访问层
 *
 * @author makejava
 * @since 2019-03-04 15:03:18
 */
public interface TbSalaryDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TbSalaryDTO queryById(String id);
    
    /**
     * 条件查询数据
     * @param tbSalary 参数
     * @return 实例对象
     */    
    TbSalaryDTO queryByParam(TbSalaryDTO tbSalary);

    /**
     * 查询指定行数据
     * @param  tbSalary
     * @return 对象列表
     */
    List<TbSalaryDTO> queryAllByLimit(TbSalaryDTO tbSalary);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param tbSalary 实例对象
     * @return 对象列表
     */
    List<TbSalaryDTO> queryAll(TbSalaryDTO tbSalary);

    /**
     * 新增数据
     *
     * @param tbSalary 实例对象
     * @return 影响行数
     */
    int insert(TbSalaryDTO tbSalary);

    /**
     * 修改数据
     *
     * @param tbSalary 实例对象
     * @return 影响行数
     */
    int update(TbSalaryDTO tbSalary);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

}