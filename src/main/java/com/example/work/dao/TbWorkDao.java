package com.example.work.dao;

import com.example.work.entity.TbWorkDTO;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (TbWork)表数据库访问层
 *
 * @author makejava
 * @since 2019-03-04 15:03:20
 */
public interface TbWorkDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TbWorkDTO queryById(String id);

    /**
     * 根据用户ID查询
     * @param userId
     * @return
     */
    List<TbWorkDTO> queryByUserId(@Param("userId") String userId);
    
    /**
     * 条件查询数据
     * @param tbWork 参数
     * @return 实例对象
     */    
    TbWorkDTO queryByParam(TbWorkDTO tbWork);

    /**
     * 查询指定行数据
     *
     * @param tbWork 查询起始位置
     * @return 对象列表
     */
    List<TbWorkDTO> queryAllByLimit(TbWorkDTO tbWork);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param tbWork 实例对象
     * @return 对象列表
     */
    List<TbWorkDTO> queryAll(TbWorkDTO tbWork);

    /**
     * 新增数据
     *
     * @param tbWork 实例对象
     * @return 影响行数
     */
    int insert(TbWorkDTO tbWork);

    /**
     * 修改数据
     *
     * @param tbWork 实例对象
     * @return 影响行数
     */
    int update(TbWorkDTO tbWork);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

}