package com.example.work.dao;

import com.example.work.entity.TbWorkCountDTO;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (TbWorkCount)表数据库访问层
 *
 * @author makejava
 * @since 2019-03-04 15:03:21
 */
public interface TbWorkCountDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TbWorkCountDTO queryById(String id);
    
    /**
     * 条件查询数据
     * @param tbWorkCount 参数
     * @return 实例对象
     */    
    TbWorkCountDTO queryByParam(TbWorkCountDTO tbWorkCount);

    /**
     * 查询指定行数据
     *
     * @param tbWorkCount 查询起始位置
     * @return 对象列表
     */
    List<TbWorkCountDTO> queryAllByLimit(TbWorkCountDTO tbWorkCount);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param tbWorkCount 实例对象
     * @return 对象列表
     */
    List<TbWorkCountDTO> queryAll(TbWorkCountDTO tbWorkCount);

    /**
     * 新增数据
     *
     * @param tbWorkCount 实例对象
     * @return 影响行数
     */
    int insert(TbWorkCountDTO tbWorkCount);

    /**
     * 修改数据
     *
     * @param tbWorkCount 实例对象
     * @return 影响行数
     */
    int update(TbWorkCountDTO tbWorkCount);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

}