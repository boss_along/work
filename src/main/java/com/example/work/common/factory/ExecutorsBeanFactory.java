package com.example.work.common.factory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

/**
 * @author Thinkpad
 * @Title: ExecutorsBeanFactory
 * @ProjectName work
 * @Description: TODO
 * @date 2019/3/2914:05
 * @Version: 1.0
 */
public class ExecutorsBeanFactory {

    private static Map<Integer, Function<Integer,Object>> map = new HashMap<>();

    static {
        map.put(1,(t)->{return t;});
        map.put(2,(t)->{return "222";});

        map = Collections.unmodifiableMap(map);
    }

    private ExecutorsBeanFactory(){}

    public static void main(String[] args) {
        Object apply1 = map.get(1).apply(22222);
        Object apply2 = map.get(2).apply(2);
        System.out.println(apply1);
        System.out.println(apply2);
    }
}
