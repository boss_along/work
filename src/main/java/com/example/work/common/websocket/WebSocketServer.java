package com.example.work.common.websocket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * @author Thinkpad
 * @Title: WebSocketServer
 * @ProjectName work
 * @Description: TODO
 * @date 2019/3/2610:54
 * @Version: 1.0
 */
@ServerEndpoint("/websocket/{sid}")
@Component
@Slf4j
public class WebSocketServer {

    private Session session;

    private static CopyOnWriteArraySet<WebSocketServer> websocketSet = new CopyOnWriteArraySet<>();

    private static BlockingDeque inQueue = new LinkedBlockingDeque(1024);

    private static BlockingDeque outQueue = new LinkedBlockingDeque(1024);

    /**
     * 开启
     * @param session
     */
    @OnOpen
    public void onOpen(Session session){
        String name = Thread.currentThread().getName();
        log.info(">>>>>>>>>>>>>>>开启线程-当前线程名称:"+name+"<<<<<<<<<<<<<<<<<<<<");
        this.session = session;
        websocketSet.add(this);


    }


    /**
     * 关闭
     */
    @OnClose
    public void onClose(){
        log.info(">>>>>>>>>>>>>>>>>>>>>socket关闭<<<<<<<<<<<<<<<<<<<<<<");
        websocketSet.remove(this);
    }

    /**
     * 接收消息
     * @param message
     * @param session
     */
    @OnMessage
    public void onMessage(String message,Session session){
        String name = Thread.currentThread().getName();
        log.info(">>>>>>>>>>>>>>>>>接受消息-当前线程名称"+name+"<<<<<<<<<<<<<<<<<<<<");
        if(StringUtils.isEmpty(message)){
            log.debug(">>>>>>>>>>>>>>>>>消息为空<<<<<<<<<<<<<<<<<<<<<<<<");
            return;
        }
        try {
            inQueue.put(message);
        } catch (InterruptedException e) {
            log.error(">>>>>>>>>>>>>>>>>error-当前线程名称"+name+"<<<<<<<<<<<<<<<<<<<<",e);
        }
    }


    @OnError
    public void onError(Session session, Throwable error){
        log.error("发生错误",error);
    }

    /**
     * 实现服务器主动推送
     */
    public void sendMessage(String message) throws IOException {
        this.session.getBasicRemote().sendText(message);
    }

}
