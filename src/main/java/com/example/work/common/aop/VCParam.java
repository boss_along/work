package com.example.work.common.aop;

import java.lang.annotation.*;

/**
 * @ClassName: VCParam
 * @Description: 版本控制锁-参数注解
 * 使用了该注解的参数，必须为自定义类的对象，且含有VCIdField、VCVersionField两个注解属性
 * @Author: bosslong
 * @Date: 2019/3/6 14:06
 */
@Target({ ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@interface VCParam {
}
