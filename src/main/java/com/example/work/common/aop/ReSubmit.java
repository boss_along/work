package com.example.work.common.aop;

import com.example.work.common.annotation.TestAnnotation;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Thinkpad
 * @Title: ReSubmit
 * @ProjectName work
 * @Description: TODO
 * @date 2019/3/617:35
 * @Version: 1.0
 */
//@Aspect
//@Component
//@Slf4j
public class ReSubmit {




    //@Around(value="(execution(* com.example.work.service.*.*(..))  && @annotation(test))")
    public void arround(ProceedingJoinPoint joinPoint, TestAnnotation test) throws Throwable {
        ServletRequestAttributes attributes = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        Object[] args = joinPoint.getArgs();
        String kind = joinPoint.getKind();
        String desc = test.desc();
        String table = test.table();
        int a = 1/0;
        Object proceed = joinPoint.proceed();
    }

}
