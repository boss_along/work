package com.example.work.common.aop;

import java.lang.annotation.*;

/**
 * @ClassName: VCIdField
 * @Description: 版本控制锁-ID字段注解
 * 使用了该注解的字段将会被视作id
 * @Author: bosslong
 * @Date: 2019/3/6 14:06
 */
@Target({ ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface VCIdField {
}
