package com.example.work.common.aop;

import com.example.work.common.annotation.VSLock;
import com.example.work.dao.TbTestDao;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @author Thinkpad
 * @Title: VSLockAop
 * @ProjectName work
 * @Description: TODO
 * @date 2019/3/2110:52
 * @Version: 1.0
 */
@Component
@Aspect
@Slf4j
@Order(value = Integer.MAX_VALUE)
public class VSLockAop {


    @Autowired
    private DataSourceTransactionManager dataSourceTransactionManager;

    @Autowired
    private TbTestDao tbTestDao;

    @Pointcut("@annotation(vsLock)")
    public void pointCut(VSLock vsLock){}

    @Around("pointCut(vsLock)")
    public Object arround(ProceedingJoinPoint joinPoint, VSLock vsLock) throws Throwable{
        log.info(">>>>>>>>>>>>>>>>>>>版本控制切面start...<<<<<<<<<<<<<<<<<<<<<<<");
        TransactionTemplate transactionTemplate = new TransactionTemplate(dataSourceTransactionManager);
        transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        transactionTemplate.setName(joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName()+"-vcLock");
        return transactionTemplate.execute((TransactionCallback) status -> execute(joinPoint));
    }


    private Object execute(ProceedingJoinPoint point) {
        log.info("进入版本控制锁处理切面,当前所处事务:{}", TransactionSynchronizationManager.getCurrentTransactionName());
        try {
            //获取id/version/tenantId/tenantIdField
            ParameterHandler parameterHandler = new ParameterHandler(point).invoke();
            Integer version = parameterHandler.getVersion();
            String id = parameterHandler.getId();
            String tenantId = parameterHandler.getTenantId();
            String tenantIdField = parameterHandler.getTenantIdField();
            if(StringUtils.isBlank(tenantId)){
                throw new RuntimeException("tenantId不可为空");
            }
            //若id为空说明是新增操作，不需要做处理
            if (StringUtils.isBlank(id)) {
                return point.proceed(point.getArgs());
            }
            if (version == null) {
                throw new RuntimeException("版本控制不可为空");
            }
            String table = getTable(point);
            String tenantCondition = String.format("%s = '%s'", tenantIdField, tenantId);
            int update = tbTestDao.update(table, id, version.toString(), tenantCondition);
            if (update < 1) {
                throw new RuntimeException("单据已变更，请刷新重试");
            }
            return point.proceed(point.getArgs());
        } catch (RuntimeException e){
            throw e;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            throw new RuntimeException("系统错误");
        }
    }

    /**
     * 获取分布式锁注解中的参数
     *
     * @param joinPoint
     * @return
     */
    private String getTable(JoinPoint joinPoint) {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        return method.getAnnotation(VSLock.class).tbName();
    }


    /**
     * 获取参数内部类
     */
    private class ParameterHandler {
        private ProceedingJoinPoint point;
        private Integer version;
        private String id;
        private String tenantId;
        private String tenantIdField;

        public ParameterHandler(ProceedingJoinPoint point) {
            this.point = point;
        }

        public Integer getVersion() {
            return version;
        }

        public String getId() {
            return id;
        }

        public String getTenantId() {
            return tenantId;
        }

        public String getTenantIdField() {
            return tenantIdField;
        }

        public ParameterHandler invoke() throws IllegalAccessException {
            //获取参数对象
            Object[] args = point.getArgs();
            //获取方法参数
            MethodSignature signature = (MethodSignature) point.getSignature();
            Parameter[] parameters = signature.getMethod().getParameters();

            for (int i = 0; i < parameters.length; i++) {
                Parameter parameter = parameters[i];
                //只有标有VCParam注解的类，才可以执行
                if (parameter.getAnnotation(VCParam.class) == null) {
                    continue;
                }
                Class<?> paramClazz = parameter.getType();
                //获取类型所对应的参数对象
                Object arg = Arrays.stream(args).filter(a -> a != null && paramClazz.isAssignableFrom(a.getClass())).findFirst().get();
                //得到参数的所有成员变量
                List<Field> fieldList = new ArrayList<>();
                Class<?> paramClazzTmp = paramClazz;
                //遍历paramClazzTmp及其所有父类，并收集所有field
                while (paramClazzTmp != null) {
                    fieldList.addAll(Arrays.asList(paramClazzTmp.getDeclaredFields()));
                    paramClazzTmp = paramClazzTmp.getSuperclass();
                }
                for (Field field : fieldList) {
                    field.setAccessible(true);
                    VCIdField vcIdField = field.getAnnotation(VCIdField.class);
                    if (vcIdField != null) {
                        Object fieldValue = field.get(arg);
                        id = (String) fieldValue;
                    }
                    VCVersionField vcVersionField = field.getAnnotation(VCVersionField.class);
                    if (vcVersionField != null) {
                        Object fieldValue = field.get(arg);
                        version = (Integer) fieldValue;
                    }
                    VCTenantIdField vcTenantIdField = field.getAnnotation(VCTenantIdField.class);
                    if (vcTenantIdField != null) {
                        Object fieldValue = field.get(arg);
                        tenantId = (String) fieldValue;
                        tenantIdField = vcTenantIdField.value();
                    }
                }
            }
            return this;
        }
    }
}
