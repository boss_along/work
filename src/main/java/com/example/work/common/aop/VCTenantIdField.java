package com.example.work.common.aop;

import java.lang.annotation.*;
/**
 * @ClassName: VCTenantIdField
 * @Description: 版本控制锁-TenantId字段注解
 * 使用了该注解的字段将会被视作tenantId
 * 表中tenantId字段名默认为tenant_id，可自定义
 * @Author: bosslong
 * @Date: 2019/3/6 14:06
 */
@Target({ ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface VCTenantIdField {

    String value() default "tenant_id";
}
