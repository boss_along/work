package com.example.work.common.listener;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * @author Thinkpad
 * @Title: InitCommandRunner
 * @ProjectName work
 * @Description: TODO
 * @date 2019/2/1814:49
 * @Version: 1.0
 */
@Component
public class InitCommandRunner implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {

        ScheduledExecutorService s = Executors.newSingleThreadScheduledExecutor();

    }

}
