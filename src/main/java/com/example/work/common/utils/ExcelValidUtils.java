package com.example.work.common.utils;

import com.example.work.common.exception.BusinessException;
import com.example.work.common.model.ErrorPosition;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * @author Thinkpad
 * @Title: ExcelValidUtils
 * @ProjectName work
 * @Description: TODO
 * @date 2019/3/1915:06
 * @Version: 1.0
 */
public class ExcelValidUtils {
    /**
     * 生成Workbook
     * @param multipartFile
     * @return
     */
    public static Workbook createWorkbook(MultipartFile multipartFile){
        try {
            return new HSSFWorkbook(multipartFile.getInputStream());
        } catch (Exception e) {
            try {
                return new XSSFWorkbook(multipartFile.getInputStream());
            } catch (IOException e1) {
                throw new BusinessException(e1);
            }
        }
    }
}
