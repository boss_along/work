package com.example.work.common.utils;

import com.example.work.entity.TbUserDTO;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Thinkpad
 * @Title: LoginUserUtils
 * @ProjectName work
 * @Description: TODO
 * @date 2019/2/1715:49
 * @Version: 1.0
 */
public class LoginUserUtils {

    /**
     * 获取登陆用户信息
     * @return
     */
    public static TbUserDTO getUserInfo(){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return (TbUserDTO)request.getSession().getAttribute("loginUser");
    }

    /**
     * 获取用户名
     * @return
     */
    public static String getUsername(){
        return getUserInfo().getUsername();
    }
}
