package com.example.work.common.utils;

import com.example.work.common.model.ErrorPosition;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Thinkpad
 * @Title: ExcelValidateWarpper
 * @ProjectName work
 * @Description: TODO
 * @date 2019/3/1915:14
 * @Version: 1.0
 */
public class ExcelValidateWarpper {

    private Set<ErrorPosition> errInfo  = new HashSet<>();

    private Workbook workbook;

    private String standardTitles[];

    public ExcelValidateWarpper(Workbook workbook,String standardTitles[]) {
        if(Objects.isNull(workbook)||standardTitles==null || standardTitles.length <1 ){
            throw new NullPointerException();
        }
        this.workbook = workbook;
        this.standardTitles = standardTitles;
    }

    public ExcelValidateWarpper(Workbook workbook) {
        if(Objects.isNull(workbook)){
            throw new NullPointerException();
        }
        this.workbook = workbook;
    }

    /**
     * sheet页校验
     */
    public ExcelValidateWarpper validSheet(){
        int numberOfSheets = workbook.getNumberOfSheets();
        if(numberOfSheets<1){
            errInfo.add(new ErrorPosition("sheet页小于0"));
        }
        return this;
    }

    /**
     * 数据数量校验
     */
    public ExcelValidateWarpper validDataCount(){
        Sheet sheet0 = workbook.getSheetAt(0);
        int physicalNumberOfRows = sheet0.getPhysicalNumberOfRows();
        if(physicalNumberOfRows<2){
            errInfo.add(new ErrorPosition("当前sheet页无数据"));
        }
        return this;
    }

    /**
     * excel标题校验
     * @return
     */
    public ExcelValidateWarpper validTitle(){
        Row row0 = workbook.getSheetAt(0).getRow(0);

        int physicalNumberOfCells = row0.getPhysicalNumberOfCells();

        List<String> excelTitles = new ArrayList<>();

        for(int i=0;i<physicalNumberOfCells;i++){
            Cell cell = row0.getCell(i);
            excelTitles.add(cell.getStringCellValue());
        }

        List<String> asList = Arrays.asList(standardTitles);
        if(asList.size()!=excelTitles.size()){
            errInfo.add(new ErrorPosition("excel标题不正确"));
        }
        for(int i=0;i<asList.size();i++){
            String standardTitle = asList.get(i);
            String excelTitle = excelTitles.get(i);
            if(!standardTitle.equals(excelTitle)){
                errInfo.add(new ErrorPosition(1,i+1,"excel标题不正确"));
            }
        }
        return this;
    }

    /**
     * 清空错误信息
     * @return
     */
    public ExcelValidateWarpper errInfoEmpty(){
        errInfo.clear();
        return this;
    }

    /**
     * 获取错误信息（排序）
     * @return
     */
    public List<ErrorPosition> getSortedErrInfo(){
        List<ErrorPosition> list = getErrInfo().stream().sorted(Comparator.comparing((ErrorPosition::getRow)).thenComparing(Comparator.comparing(ErrorPosition::getColumn))).collect(Collectors.toList());
        return list;
    }

    /**
     * 获取错误信息
     * @return
     */
    public List<ErrorPosition> getErrInfo(){
        return new ArrayList<>(errInfo);
    }



}
