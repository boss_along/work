package com.example.work.common.thread;

import java.util.Objects;
import java.util.concurrent.*;

/**
 * @author Thinkpad
 * @Title: ThreadPool
 * @ProjectName work
 * @Description: TODO
 * @date 2019/2/279:35
 * @Version: 1.0
 */
public class ThreadPool {

    private static volatile ExecutorService executorService = null;

    private ThreadPool(){}

    public static ExecutorService newInstance(){
        if(Objects.isNull(executorService)){
            synchronized (ThreadPool.class){
                if(Objects.isNull(executorService)){
                    executorService = new ThreadPoolExecutor(5,5,0L, TimeUnit.MILLISECONDS,new LinkedBlockingDeque<>(1024));
                }
            }
        }
        return executorService;
    }

}
