package com.example.work.common.enums;

public enum ExecutorEnum {
    Draft(0),Todo(1),Finish(2),Reject(3);

    private int state;

    ExecutorEnum(int state){
        this.state = state;
    }

    private int getState(){
        return  state;
    }
}
