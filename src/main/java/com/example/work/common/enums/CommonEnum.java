package com.example.work.common.enums;

import lombok.Getter;

@Getter
public enum CommonEnum {

    PARAM_ERROR(10001,"参数错误");

    /** 数据库编码 */
    private Integer code;
    /** 中文意思 */
    private String msg;

    CommonEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
