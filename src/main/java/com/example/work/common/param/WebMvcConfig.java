package com.example.work.common.param;

import com.example.work.common.validate.DateConveter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Thinkpad
 * @Title: WebMvcConfig
 * @ProjectName work
 * @Description: TODO
 * @date 2019/4/1914:20
 * @Version: 1.0
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Autowired
    private DateConveter dateConveter;

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(dateConveter);
    }
}
