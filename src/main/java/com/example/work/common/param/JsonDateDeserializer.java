package com.example.work.common.param;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Thinkpad
 * @Title: JsonDateDeserializer
 * @ProjectName work
 * @Description: TODO
 * @date 2019/4/1916:38
 * @Version: 1.0
 */
@Slf4j
public class JsonDateDeserializer extends JsonDeserializer {

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        log.info(">>>>>>>>>>>>>>>执行json日期转换器start...<<<<<<<<<<<<<<<<");
        String valueAsString = jsonParser.getValueAsString();
        if(StringUtils.isEmpty(valueAsString)){
            return null;
        }
        try {
            Date result = sdf.parse(valueAsString);
            log.info(">>>>>>>>>>>>>>>执行json日期转换器over...<<<<<<<<<<<<<<<<");
            return result;
        } catch (ParseException e) {
            log.error(">>>>>>>>>>>>>>>>>日期参数格式化异常<<<<<<<<<<<<<<<<",e);
        }
        return null;
    }
}
