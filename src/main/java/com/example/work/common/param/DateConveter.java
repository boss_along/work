package com.example.work.common.param;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Thinkpad
 * @Title: DateConveter
 * @ProjectName work
 * @Description: TODO
 * @date 2019/4/1914:16
 * @Version: 1.0
 */
@Component
@Slf4j
public class DateConveter implements Converter<String, Date> {

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public Date convert(String s) {
        if(StringUtils.isEmpty(s)){
            return null;
        }
        try {
            return sdf.parse(s);
        } catch (ParseException e) {
            log.error(">>>>>>>>>>>>>日期参数绑定器格式化日期异常<<<<<<<<<<<<<<",e);
        }
        return null;
    }
}
