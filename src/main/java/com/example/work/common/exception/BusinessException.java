package com.example.work.common.exception;

/**
 * @author Thinkpad
 * @Title: BusinessException
 * @ProjectName work
 * @Description: TODO
 * @date 2019/3/1914:27
 * @Version: 1.0
 */
public class BusinessException extends RuntimeException {

    private Integer errCode;

    public BusinessException(Integer errCode, String message, Throwable cause){
        super(message, cause);
        this.errCode=errCode;
    }

    public BusinessException( String message) {
        super(message);
    }

    public BusinessException(Integer errCode, String message) {
        super(message);
        this.errCode=errCode;
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }
}
