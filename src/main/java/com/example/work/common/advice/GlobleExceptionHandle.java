package com.example.work.common.advice;

import com.example.work.common.enums.CommonEnum;
import com.example.work.common.model.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Thinkpad
 * @Title: GlobleExceptionHandle
 * @ProjectName work
 * @Description: TODO
 * @date 2019/2/1514:45
 * @Version: 1.0
 */
@ControllerAdvice
@Slf4j
public class GlobleExceptionHandle {

    /**
     * 应用到所有@RequestMapping注解方法，在其执行之前初始化数据绑定器
     * @param binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        System.out.println("------------------");
    }

    /**
     * 把值绑定到Model中，使全局@RequestMapping可以获取到该值
     * @param model
     */
    @ModelAttribute
    public void addAttributes(Model model) {
        model.addAttribute("author", "Magical Sam");
    }

    /**
     * 全局异常捕捉处理
     * @param ex
     * @return
     */
    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public Map globleExceptionHandler(Exception ex) {
        Map map = new HashMap<>(16);
        ex.printStackTrace();
        map.put("code", 100);
        map.put("msg", ex.getMessage());
        return map;
    }

    @ResponseBody
    @ExceptionHandler(value = javax.validation.ConstraintViolationException.class)
    public Result paramExceptionHandler(Exception ex) {
        return Result.error(CommonEnum.PARAM_ERROR.getCode(),ex.getMessage());
    }
}
