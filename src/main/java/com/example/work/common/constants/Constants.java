package com.example.work.common.constants;

/**
 * @author Thinkpad
 * @Title: Constants
 * @ProjectName work
 * @Description: TODO
 * @date 2019/3/414:27
 * @Version: 1.0
 */
public class Constants {

    /** 打卡類別-上班卡 */
    public static final String SYS_CARD_TYPE_ARRIVE = "上班卡";

    /** 打卡類別-下班卡 */
    public static final String SYS_CARD_TYPE_LEAVE = "下班卡";

    /** 打卡状态-正常 */
    public static final String SYS_CARD_STATUS_NORMAL = "正常";

    /** 打卡状态-迟到 */
    public static final String SYS_CARD_STATUS_LATER = "迟到";

    /** 打卡状态-早退 */
    public static final String SYS_CARD_STATUS_EARLY = "早退";

    /** 是否标志-是 */
    public static final String SYS_ISORNOT_IS = "是";
    /** 是否标志-否 */
    public static final String SYS_ISORNOT_NOT = "否";

    /** 是否标志-是 */
    public static final String SYS_STATUS_NORMAL = "正常";
    /** 是否标志-否 */
    public static final String SYS_STATUS_NOT = "";
}
