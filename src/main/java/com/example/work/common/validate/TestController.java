package com.example.work.common.validate;

import com.example.work.common.validate.groups.Add;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author Thinkpad
 * @Title: TestController
 * @ProjectName choice-produce
 * @Description: TODO
 * @date 2019/4/1810:57
 * @Version: 1.0
 */
@RequestMapping("/test")
@RestController
@Slf4j
public class TestController {

    @RequestMapping("/add")
    public Object test(@RequestBody Student stu){

        return "success";
    }
}
