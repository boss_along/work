package com.example.work.common.validate;

import com.example.work.common.validate.groups.Add;
import com.example.work.common.validate.groups.Delete;
import com.example.work.common.validate.groups.Update;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Thinkpad
 * @Title: Student
 * @ProjectName choice-produce
 * @Description: TODO
 * @date 2019/4/1810:49
 * @Version: 1.0
 */
@Data
public class Student {

    /*@NotNull(message = "主键不能为空",groups = {Update.class, Delete.class})
    private String id;

    @Length(min = 5,max = 20,message = "名称长度不符合规范",groups = {Add.class,Update.class})
    @Pattern(regexp = "[a-zA-Z]{5,20}",message = "名称内容不符合规范",groups = {Add.class,Update.class})
    private String name;

    @NotNull(message = "密码不能为空a",groups = {Update.class, Add.class})
    private String password;*/

    @JsonDeserialize(using = JsonDateDeserializer.class)
    private Date date;

}
