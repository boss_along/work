package com.example.work.common.task;

import com.example.work.entity.TbUserDTO;
import com.example.work.entity.TbWorkDTO;
import com.example.work.service.TbUserService;
import com.example.work.service.TbWorkService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Thinkpad
 * @Title: ScheduledService
 * @ProjectName work
 * @Description: TODO
 * @date 2019/2/1815:08
 * @Version: 1.0
 */
@Slf4j
@Component
public class ScheduledService {

    @Autowired
    private TbWorkService tbWorkService;

    @Autowired
    private TbUserService tbUserService;

    @Async
    @Scheduled(cron = "00 00 00 * * * ")
    public void scheduled(){
        List<TbUserDTO> list = tbUserService.queryAll(new TbUserDTO());

        List<TbWorkDTO> workList = new ArrayList<>();

        list.forEach(ele ->{
            for(int i = 0 ; i<2;i++){
                TbWorkDTO tbWorkDTO = new TbWorkDTO();
                tbWorkDTO.setId(UUID.randomUUID().toString());
                tbWorkDTO.setUserId(ele.getId());
                //未打卡
                tbWorkDTO.setWorkStatus("0");
                //打卡类型 0上班卡 1下班卡
                tbWorkDTO.setType(i+"");
                workList.add(tbWorkDTO);
            }
        });
        tbWorkService.batchInsert(workList);
        log.info("=====>>>>>使用cron  {}",System.currentTimeMillis());
    }

    /*@Async
    @Scheduled(fixedRate = 5000)
    public void scheduled1() {
        log.info("=====>>>>>使用fixedRate{}", System.currentTimeMillis());
    }

    @Async
    @Scheduled(fixedDelay = 5000)
    public void scheduled2() {
        log.info("=====>>>>>fixedDelay{}",System.currentTimeMillis());
    }
*/}
