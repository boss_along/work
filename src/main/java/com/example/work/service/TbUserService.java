package com.example.work.service;

import com.example.work.entity.TbUserDTO;
import java.util.List;

/**
 * (TbUser)表服务接口
 *
 * @author makejava
 * @since 2019-02-16 07:39:54
 */
public interface TbUserService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TbUserDTO queryById(String id);


    /**
     *
     * @param id
     * @return
     */
    List<TbUserDTO> queryListById(String id);

    /**
     * 通过用户名查询用户信息
     * @param username
     * @return
     */
    TbUserDTO queryByUsername(String username,String password,String role);
    /**
     * 查询多条数据
     *
     * @param tbUserDTO 查询参数
     * @return 对象列表
     */
    List<TbUserDTO> queryAllByLimit(TbUserDTO tbUserDTO);

    /**
     * 查询所有
     * @param tbUserDTO
     * @return
     */
    List<TbUserDTO> queryAll( TbUserDTO tbUserDTO);

    /**
     * 新增数据
     *
     * @param tbUser 实例对象
     * @return 实例对象
     */
    TbUserDTO insert(TbUserDTO tbUser);

    /**
     * 修改数据
     *
     * @param tbUser 实例对象
     * @return 实例对象
     */
    TbUserDTO update(TbUserDTO tbUser);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

}