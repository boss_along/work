package com.example.work.service;

import com.example.work.entity.TbSalaryDTO;
import java.util.List;

/**
 * (TbSalary)表服务接口
 *
 * @author makejava
 * @since 2019-02-16 07:39:53
 */
public interface TbSalaryService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TbSalaryDTO queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<TbSalaryDTO> queryAllByLimit(TbSalaryDTO tbSalary);

    /**
     * 新增数据
     *
     * @param tbSalary 实例对象
     * @return 实例对象
     */
    TbSalaryDTO insert(TbSalaryDTO tbSalary);

    /**
     * 修改数据
     *
     * @param tbSalary 实例对象
     * @return 实例对象
     */
    TbSalaryDTO update(TbSalaryDTO tbSalary);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

}