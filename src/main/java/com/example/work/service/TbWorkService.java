package com.example.work.service;

import com.example.work.entity.TbWorkCountDTO;
import com.example.work.entity.TbWorkDTO;
import java.util.List;

/**
 * (TbWork)表服务接口
 *
 * @author makejava
 * @since 2019-02-16 07:39:55
 */
public interface TbWorkService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TbWorkDTO queryById(String id);

    /**
     * 用户ID
     * @param userId
     * @return
     */
    List<TbWorkDTO> queryByUserId(String userId);

    /**
     * 统计工作情况
     * @param tbWork
     */
    void count(TbWorkDTO tbWork);

    /**
     * 查询多条数据
     *
     * @param tbWork 查询起始位置
     * @return 对象列表
     */
    List<TbWorkDTO> queryAllByLimit(TbWorkDTO tbWork);

    /**
     * 新增数据
     *
     * @param tbWork 实例对象
     * @return 实例对象
     */
    TbWorkDTO insert(TbWorkDTO tbWork);

    /**
     * 管理员新增数据
     * @param tbWork
     * @return
     */
    TbWorkDTO managerInsert(TbWorkDTO tbWork);

    /**
     * 批量新增
     * @param list
     * @return
     */
    List<TbWorkDTO> batchInsert(List<TbWorkDTO> list);

    /**
     * 修改数据
     *
     * @param tbWork 实例对象
     * @return 实例对象
     */
    TbWorkDTO update(TbWorkDTO tbWork);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

}