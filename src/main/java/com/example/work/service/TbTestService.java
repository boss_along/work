package com.example.work.service;

import com.example.work.entity.TbTestDTO;
import java.util.List;

/**
 * (TbTest)表服务接口
 *
 * @author makejava
 * @since 2019-03-21 09:51:43
 */
public interface TbTestService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TbTestDTO queryById(Integer id);
    
    /**
     * 条件查询数据
     * @param tbTest 参数
     * @return 实例对象
     */
    TbTestDTO queryByParam(TbTestDTO tbTest);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<TbTestDTO> queryAllByLimit(TbTestDTO tbTest);

    /**
     * 新增数据
     *
     * @param tbTest 实例对象
     * @return 实例对象
     */
    TbTestDTO insert(TbTestDTO tbTest);

    /**
     * 修改数据
     *
     * @param tbTest 实例对象
     * @return 实例对象
     */
    TbTestDTO update(TbTestDTO tbTest);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

}