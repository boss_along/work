package com.example.work.service;

import com.example.work.entity.TbWorkCountDTO;
import java.util.List;

/**
 * (TbWorkCount)表服务接口
 *
 * @author makejava
 * @since 2019-02-16 07:39:57
 */
public interface TbWorkCountService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TbWorkCountDTO queryById(String id);

    /**
     * 查询多条数据
     *
     * @param tbWorkCount 查询起始位置
     * @return 对象列表
     */
    List<TbWorkCountDTO> queryAllByLimit(TbWorkCountDTO tbWorkCount);

    /**
     * 新增数据
     *
     * @param tbWorkCount 实例对象
     * @return 实例对象
     */
    TbWorkCountDTO insert(TbWorkCountDTO tbWorkCount);

    /**
     * 修改数据
     *
     * @param tbWorkCount 实例对象
     * @return 实例对象
     */
    TbWorkCountDTO update(TbWorkCountDTO tbWorkCount);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

}