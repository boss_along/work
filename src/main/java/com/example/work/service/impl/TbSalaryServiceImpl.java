package com.example.work.service.impl;

import com.example.work.common.utils.LoginUserUtils;
import com.example.work.entity.TbSalaryDTO;
import com.example.work.dao.TbSalaryDao;
import com.example.work.service.TbSalaryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * (TbSalary)表服务实现类
 *
 * @author makejava
 * @since 2019-02-16 07:39:53
 */
@Service("tbSalaryService")
public class TbSalaryServiceImpl implements TbSalaryService {
    @Resource
    private TbSalaryDao tbSalaryDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public TbSalaryDTO queryById(String id) {
        return tbSalaryDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param tbSalary 查询起始位置
     * @return 对象列表
     */
    @Override
    public List<TbSalaryDTO> queryAllByLimit(TbSalaryDTO tbSalary) {
        return tbSalaryDao.queryAllByLimit(tbSalary);
    }

    /**
     * 新增数据
     *
     * @param tbSalary 实例对象
     * @return 实例对象
     */
    @Override
    public TbSalaryDTO insert(TbSalaryDTO tbSalary) {
        tbSalary.setId(UUID.randomUUID().toString());
        tbSalary.setCreater(LoginUserUtils.getUsername());
        tbSalary.setCreateTime(new Date());
        tbSalaryDao.insert(tbSalary);
        return tbSalary;
    }

    /**
     * 修改数据
     *
     * @param tbSalary 实例对象
     * @return 实例对象
     */
    @Override
    public TbSalaryDTO update(TbSalaryDTO tbSalary) {
        tbSalaryDao.update(tbSalary);
        return queryById(tbSalary.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return tbSalaryDao.deleteById(id) > 0;
    }
}