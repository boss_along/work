package com.example.work.service.impl;

import com.example.work.common.annotation.TestAnnotation;
import com.example.work.common.annotation.VSLock;
import com.example.work.common.utils.LoginUserUtils;
import com.example.work.entity.TbUserDTO;
import com.example.work.dao.TbUserDao;
import com.example.work.service.TbUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.beans.Transient;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * (TbUser)表服务实现类
 *
 * @author makejava
 * @since 2019-02-16 07:39:54
 */
@Service("tbUserService")
public class TbUserServiceImpl implements TbUserService {
    @Resource
    private TbUserDao tbUserDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public TbUserDTO queryById(String id) {
        return tbUserDao.queryById(id);
    }

    @Override
    @VSLock(tbName = "tb_test")
    @Transactional
    public List<TbUserDTO> queryListById(String id) {
        return tbUserDao.queryListById(id);
    }

    @Override
    public TbUserDTO queryByUsername(String username,String password,String role) {

        TbUserDTO tbUserDTO = tbUserDao.queryByUserName(username);

        if(Objects.isNull(tbUserDTO)){
            return null;
        }

        String pwd = tbUserDTO.getPassword();
        if(!password.equals(pwd)){
            return null;
        }

        if(!role.equals(tbUserDTO.getRole())){
            return null;
        }

        return tbUserDTO;
    }

    /**
     * 查询多条数据
     *
     * @param tbUserDTO 查询参数
     * @return 对象列表
     */
    @Override
    public List<TbUserDTO> queryAllByLimit(TbUserDTO tbUserDTO) {
        return tbUserDao.queryAllByLimit(tbUserDTO);
    }

    @Override
    public List<TbUserDTO> queryAll(TbUserDTO tbUserDTO) {
        return tbUserDao.queryAll(tbUserDTO);
    }

    /**
     * 新增数据
     *
     * @param tbUser 实例对象
     * @return 实例对象
     */
    @Override
    public TbUserDTO insert(TbUserDTO tbUser) {
        tbUser.setId(UUID.randomUUID().toString());
        tbUser.setCreater(LoginUserUtils.getUsername());
        tbUser.setCreateTime(new Date());
        tbUserDao.insert(tbUser);
        return tbUser;
    }

    /**
     * 修改数据
     *
     * @param tbUser 实例对象
     * @return 实例对象
     */
    @Transactional
    @TestAnnotation(table = "a" ,desc = "nihao")
    @Override
    public TbUserDTO update(TbUserDTO tbUser) {
        //tbUser.setModifier(LoginUserUtils.getUsername());
        tbUser.setModifyTime(new Date());
        tbUserDao.update(tbUser);
        return tbUser;
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return tbUserDao.deleteById(id) > 0;
    }
}