package com.example.work.service.impl;

import com.example.work.entity.TbTestDTO;
import com.example.work.dao.TbTestDao;
import com.example.work.service.TbTestService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * (TbTest)表服务实现类
 *
 * @author makejava
 * @since 2019-03-21 09:51:43
 */
@Service
public class TbTestServiceImpl implements TbTestService {
    @Autowired
    private TbTestDao tbTestDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public TbTestDTO queryById(Integer id) {
        return tbTestDao.queryById(id);
    }

    @Override
    public TbTestDTO queryByParam(TbTestDTO tbTest) {
        return null;
    }

    @Override
    public List<TbTestDTO> queryAllByLimit(TbTestDTO tbTest) {
        return null;
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */

    public List<TbTestDTO> queryAllByLimit(int offset, int limit) {
        return null;
    }

    /**
     * 新增数据
     *
     * @param tbTest 实例对象
     * @return 实例对象
     */
    @Override
    public TbTestDTO insert(TbTestDTO tbTest) {
        tbTestDao.insert(tbTest);
        return tbTest;
    }

    /**
     * 修改数据
     *
     * @param tbTest 实例对象
     * @return 实例对象
     */
    @Override
    public TbTestDTO update(TbTestDTO tbTest) {
        //tbTestDao.update(tbTest);
        return queryById(tbTest.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return tbTestDao.deleteById(id) > 0;
    }
}