package com.example.work.service.impl;

import com.example.work.common.constants.Constants;
import com.example.work.common.utils.LoginUserUtils;
import com.example.work.entity.TbWorkCountDTO;
import com.example.work.dao.TbWorkCountDao;
import com.example.work.service.TbWorkCountService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * (TbWorkCount)表服务实现类
 *
 * @author makejava
 * @since 2019-02-16 07:39:58
 */
@Service("tbWorkCountService")
public class TbWorkCountServiceImpl implements TbWorkCountService {
    @Resource
    private TbWorkCountDao tbWorkCountDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public TbWorkCountDTO queryById(String id) {
        return tbWorkCountDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param tbWorkCount 查询起始位置
     * @return 对象列表
     */
    @Override
    public List<TbWorkCountDTO> queryAllByLimit(TbWorkCountDTO tbWorkCount) {
        return tbWorkCountDao.queryAllByLimit(tbWorkCount);
    }



    /**
     * 新增数据
     *
     * @param tbWorkCount 实例对象
     * @return 实例对象
     */
    @Override
    public TbWorkCountDTO insert(TbWorkCountDTO tbWorkCount) {
        tbWorkCount.setId(UUID.randomUUID().toString());
        tbWorkCount.setCreateTime(new Date());
        tbWorkCount.setCreater(LoginUserUtils.getUsername());
        tbWorkCount.setStatus(Constants.SYS_CARD_STATUS_NORMAL);
        tbWorkCountDao.insert(tbWorkCount);
        return tbWorkCount;
    }

    /**
     * 修改数据
     *
     * @param tbWorkCount 实例对象
     * @return 实例对象
     */
    @Override
    public TbWorkCountDTO update(TbWorkCountDTO tbWorkCount) {
        tbWorkCountDao.update(tbWorkCount);
        return tbWorkCount;
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return tbWorkCountDao.deleteById(id) > 0;
    }
}