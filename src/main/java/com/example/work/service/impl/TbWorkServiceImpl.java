package com.example.work.service.impl;

import com.example.work.common.constants.Constants;
import com.example.work.common.utils.LoginUserUtils;
import com.example.work.dao.TbWorkCountDao;
import com.example.work.entity.TbWorkCountDTO;
import com.example.work.entity.TbWorkDTO;
import com.example.work.dao.TbWorkDao;
import com.example.work.service.TbWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * (TbWork)表服务实现类
 *
 * @author makejava
 * @since 2019-02-16 07:39:56
 */
@Service("tbWorkService")
public class TbWorkServiceImpl implements TbWorkService {

    private static final String ARRIVE_LATER_TATAL_KEY = "arriveLater";

    private static final String LEAVE_EARLY_TATAL_KEY = "leaveEarly";

    private static final String REST_TATAL_KEY = "leaveEarly";

    @Resource
    private TbWorkDao tbWorkDao;

    @Autowired
    private TbWorkCountDao tbWorkCountDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public TbWorkDTO queryById(String id) {
        return tbWorkDao.queryById(id);
    }

    @Override
    public List<TbWorkDTO> queryByUserId(String userId) {
        return tbWorkDao.queryByUserId(userId);
    }

    /**
     * 查询多条数据
     *
     * @param tbWork 查询起始位置
     * @return 对象列表
     */
    @Override
    public List<TbWorkDTO> queryAllByLimit(TbWorkDTO tbWork) {
        return tbWorkDao.queryAllByLimit(tbWork);
    }

    @Override
    public void count(TbWorkDTO tbWork) {
        List<TbWorkDTO> list = tbWorkDao.queryAllByLimit(tbWork);
        Map<String, List<TbWorkDTO>> groupByUserMap = list.stream().collect(Collectors.groupingBy(TbWorkDTO::getUserId));

        Map<String,Long> totalMap = new HashMap<>();
        groupByUserMap.forEach((k,v) -> {
            Map<String, Long> arriveLaterMap = v.stream().collect(Collectors.groupingBy(TbWorkDTO::getIsArriveLater, Collectors.counting()));
            Map<String, Long> leaveEarlyMap = v.stream().collect(Collectors.groupingBy(TbWorkDTO::getIsLeaveEarly, Collectors.counting()));
            Map<String, Long> workStatusMap = v.stream().collect(Collectors.groupingBy(TbWorkDTO::getWorkStatus, Collectors.counting()));
            //迟到
            totalMap.put(ARRIVE_LATER_TATAL_KEY,arriveLaterMap.get(1));
            //早退
            totalMap.put(LEAVE_EARLY_TATAL_KEY,leaveEarlyMap.get(1));
            //休息
            totalMap.put(REST_TATAL_KEY,workStatusMap.get(1));

            Date date = new Date();

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            String year = calendar.get(Calendar.YEAR)+"";

            //获取月份
            int month = calendar.get(Calendar.MONTH) + 1;
            String monthStr = month < 10 ? "0" + month : month + "";

            TbWorkCountDTO tbWorkCountDTO = new TbWorkCountDTO();
            tbWorkCountDTO.setId(UUID.randomUUID().toString());
            tbWorkCountDTO.setUserId(k);
            tbWorkCountDTO.setYear(year);
            tbWorkCountDTO.setMonth(monthStr);
            tbWorkCountDTO.setArriveLateDay(totalMap.get(ARRIVE_LATER_TATAL_KEY).intValue());
            tbWorkCountDTO.setLeaveEarly(totalMap.get(LEAVE_EARLY_TATAL_KEY).intValue());
            tbWorkCountDTO.setRestDay(totalMap.get(REST_TATAL_KEY).intValue());
            tbWorkCountDTO.setCreater(LoginUserUtils.getUsername());
            tbWorkCountDTO.setCreateTime(date);
            tbWorkCountDao.insert(tbWorkCountDTO);

        });



    }

    /**
     * 新增数据
     * 定時任務已經添加，
     * 打卡只是查詢當天的上班，或者下班的卡取更新
     * @param tbWork 实例对象
     * @return 实例对象
     */
    @Override
    public TbWorkDTO insert(TbWorkDTO tbWork) {
        Date date = new Date();

        tbWork.setCardTime(date);
        List<TbWorkDTO> list = tbWorkDao.queryAll(tbWork);
        if(!list.isEmpty()){
            tbWork = list.get(0);
        }

        String type = tbWork.getType();
        tbWork.setWorkStatus(Constants.SYS_CARD_STATUS_NORMAL);

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        tbWork.setIsArriveLater(Constants.SYS_ISORNOT_NOT);
        tbWork.setIsLeaveEarly(Constants.SYS_ISORNOT_NOT);
        //上班卡
        if(Constants.SYS_CARD_TYPE_ARRIVE.equals(type)){
            try {
                Date parse = sdf.parse("08:30");
                int i = date.compareTo(parse);
                tbWork.setWorkStatus(Constants.SYS_CARD_STATUS_NORMAL);
                if(i!=-1){
                    tbWork.setWorkStatus(Constants.SYS_CARD_STATUS_LATER);
                    tbWork.setIsArriveLater(Constants.SYS_ISORNOT_IS);
                }
            } catch (ParseException e) {
                //throw e;
            }
        }
        //下班卡
        if(Constants.SYS_CARD_TYPE_LEAVE.equals(type)){
            try {
                Date parse = sdf.parse("17:30");
                int i = date.compareTo(parse);
                tbWork.setWorkStatus(Constants.SYS_CARD_STATUS_NORMAL);
                if(i==-1){
                    tbWork.setWorkStatus(Constants.SYS_CARD_STATUS_EARLY);
                    tbWork.setIsLeaveEarly(Constants.SYS_ISORNOT_IS);
                }
            } catch (ParseException e) {
                //throw e;
            }
        }

        if(list.isEmpty()){
            tbWork.setId(UUID.randomUUID().toString());
            tbWork.setCardTime(date);
            tbWork.setCreater(LoginUserUtils.getUsername());
            tbWork.setCreateTime(date);


            tbWorkDao.insert(tbWork);
            return tbWork;
        }
        tbWork.setCardTime(date);
        tbWork.setCreater(LoginUserUtils.getUsername());
        tbWork.setCreateTime(new Date());
        tbWorkDao.update(tbWork);
        return tbWork;
    }

    @Override
    public TbWorkDTO managerInsert(TbWorkDTO tbWork) {
        tbWork.setId(UUID.randomUUID().toString());
        tbWork.setCreateTime(new Date());
        tbWork.setCreater(LoginUserUtils.getUsername());
        tbWorkDao.insert(tbWork);
        return tbWork;
    }

    @Override
    public List<TbWorkDTO> batchInsert(List<TbWorkDTO> list) {
        list.forEach(ele -> tbWorkDao.insert(ele));
        return list;
    }

    /**
     * 修改数据
     *
     * @param tbWork 实例对象
     * @return 实例对象
     */
    @Override
    public TbWorkDTO update(TbWorkDTO tbWork) {
        tbWorkDao.update(tbWork);
        return queryById(tbWork.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return tbWorkDao.deleteById(id) > 0;
    }
}