<%@ page contentType="text/html; charset=utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1">
</head>
<body>
    <%--<div>
        <div class="layui-btn-group demoTable">
            <button class="layui-btn" id="add1">上班打卡</button>
            <button class="layui-btn" id="add2">下班打卡</button>
        </div>
    </div>--%>
    <table class="layui-hide" id="user_table" lay-data="{id: 'idTest'}" lay-filter="demo"></table>
<script>
    var tableIns;
    layui.use([ 'jquery','table','element', 'layer' ], function() {
        var $ = layui.$;
        var table = layui.table;
        var layer = layui.layer;
        //用戶ID
        var id = ${sessionScope.loginUser.id}
        tableIns = table.render({
            elem: '#user_table',
            url:'tbSalary/list',
            cols: [[{field:'id', width:220, title: '薪资ID', sort: false},
                {field:'userId', width:220, title: '用户ID', sort: false},
                {field:'month', width:250, title: '薪资月份', sort: false},
                {field:'amt', width:250, title: '总额', sort: false},
                {field:'status', width:180, title: '状态', sort: false}]],
            page: true,//分页
            // skin: 'line',
            size: 'xs',//表格尺寸
            even: true,//隔行变色
            limit:10,//初始每页显示条数
            limits:[10,20,30]//分页下拉
        });

        //打卡
        $("#add1").click(function(){
            var url = "tbWork/add";
            $.post(url,{id:id},function(data){
                if(data.success){
                    layer.msg(data.errmsg,{icon: 6});
                    reloadTable();
                }
            },'json');
        })

        $("#add2").click(function(){
            var url = "tbWork/add";
            $.post(url,{id:id},function(data){
                if(data.success){
                    layer.msg(data.errmsg,{icon: 6});
                    reloadTable();
                }
            },'json');
        })
    });

    window.reloadTable = function (){
        tableIns.reload({
            where: { //设定异步数据接口的额外参数，任意设
                aaaaaa: 'xxx'
                ,bbb: 'yyy'
                //…
            }
            ,page: {
                curr: 1 //重新从第 1 页开始
            }
        });
    }
</script>
</body>
</html>