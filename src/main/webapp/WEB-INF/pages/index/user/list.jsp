<%@ page contentType="text/html; charset=utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1">
</head>
<body>
<div>
    <div class="layui-btn-group demoTable">
<%--
        <button class="layui-btn" id="add">新增</button>
--%>
    </div>
</div>

<table class="layui-hide" id="user_table" lay-data="{id: 'idTest'}" lay-filter="demo"></table>
<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
</script>
<script>
    var tableIns;
    layui.use([ 'jquery','table','element', 'layer' ], function() {
        var $ = layui.$;
        var table = layui.table;
        var layer = layui.layer;
        var id = ${sessionScope.loginUser.id}
        tableIns = table.render({
            elem: '#user_table',
            url:'tbUser/selectOne/'+id,
            cols: [[{field:'id', width:180, title: '用户编号', sort: false},
                {field:'username', width:180, title: '用户名', sort: false},
                {field:'password', width:120, title: '密码', sort: false},
                {field:'role', width:180, title: '角色', sort: false},
                {field:'phone', width:90, title: '电话', sort: false},
                {field:'nick', width:150, title: '昵称', sort: false},
                {field:'status', width:100,title: '状态', minWidth: 50, sort: false},
                {fixed: 'right', width:80, align:'center', toolbar: '#barDemo'}]],
            page: true,//分页
            // skin: 'line',
            size: 'xs',//表格尺寸
            even: true,//隔行变色
            limit:10,//初始每页显示条数
            limits:[10,20,30]//分页下拉
        });

        //删除操作
        table.on('tool(demo)',function(obj){
            var data = obj.data;
            var pk = data.id;
            if(obj.event === 'edit'){
                var index = layer.open({
                    type: 2,
                    title :'用户修改',
                    area: ['30%', '60%'],
                    content: 'tbUser/toupdate/'+pk //这里content是一个普通的String
                });
            }
        });
    });

    window.reloadTable = function (){
        tableIns.reload({
            where: { //设定异步数据接口的额外参数，任意设
                aaaaaa: 'xxx'
                ,bbb: 'yyy'
                //…
            }
            ,page: {
                curr: 1 //重新从第 1 页开始
            }
        });
    }
</script>
</body>
</html>