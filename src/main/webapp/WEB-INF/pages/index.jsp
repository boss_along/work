<%@ page contentType="text/html; charset=utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <title>后台管理</title>
    <link href="css/login.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div class="login_box">
    <div class="login_l_img"><img src="images/login-img.png" /></div>
    <div class="login">
        <div class="login_logo"><a href="#"><img src="images/login_logo.png" /></a></div>
        <div class="login_name">
            <p>XX企业员工管理系统</p>
        </div>
        <form method="post">
            <input name="id" type="text"  value="用户名" onfocus="this.value=''" onblur="if(this.value==''){this.value='用户名'}">
            <span id="password_text" onclick="this.style.display='none';document.getElementById('password').style.display='block';document.getElementById('password').focus().select();" >密码</span>
            <input name="pass" type="password" id="password" style="display:none;" onblur="if(this.value==''){document.getElementById('password_text').style.display='block';this.style.display='none'};"/>
            <select name="role">
                <option value="">角色</option>
                <option value="管理员">管理员</option>
                <option value="员工">员工</option>
            </select>
            <input id="loginBTN" value="登录" style="width:100%;" type="button">
            <div style="text-align:right"><a href="javascript:void(0)" id="reg" >用户注册</a></div>
        </form>
    </div>
    <div class="copyright">某某有限公司 版权所有©2018-20XX 技术支持电话：000-00000000</div>
</div>
<div style="text-align:center;">
</div>
<script src="js/jquery-3.2.1.min.js"></script>
<script>




    var socket;
    if(typeof(WebSocket) == "undefined") {
        console.log("您的浏览器不支持WebSocket");
    }else{
        console.log("您的浏览器支持WebSocket");
        //实现化WebSocket对象，指定要连接的服务器地址与端口  建立连接
        socket = new WebSocket("ws://localhost:8081/websocket/20");
        //socket = new WebSocket("${basePath}websocket/${cid}".replace("http","ws"));
        //打开事件
        socket.onopen = function() {
            console.log("Socket 已打开");
            socket.send("你好服务器");
            //socket.send("这是来自客户端的消息" + location.href + new Date());
        };
        //获得消息事件
        socket.onmessage = function(msg) {
            console.log(msg.data);
            //发现消息进入    开始处理前端触发逻辑
        };
        //关闭事件
        socket.onclose = function() {
            console.log("Socket已关闭");
        };
        //发生了错误事件
        socket.onerror = function() {
            alert("Socket发生了错误");
            //此时可以尝试刷新页面
        }

        //离开页面时，关闭socket
        //jquery1.8中已经被废弃，3.0中已经移除
        // $(window).unload(function(){
        //     socket.close();
        //});
    }




    //登录按钮点击事件
    $("#loginBTN").click(function(){

        var user_id = $("input[name=id]").val();
        var pass = $("input[name=pass]").val();
        var role = $("select[name=role]").val();

        if(user_id==""){
            alert("用户名不能为空");
            return;
        }
        if(pass==""){
            alert("密码不能为空");
            return;
        }
        if(role==""){
            alert("角色不能为空");
            return;
        }
        var url = "tbUser/login";
        $.post(url,{username:user_id,password:pass,role:role},function(data){
            if(data.success){
                window.location.href="index?role="+role;
            }else{
                alert(data.errmsg);
            }
        },'json');
    })

    //注册按钮点击事件
    $("#reg").click(function(){
        window.location.href="/job/reg.do";
    });
</script>
</body>
</html>
