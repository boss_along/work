<%@ page contentType="text/html; charset=utf-8"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  	<link rel="stylesheet" href="css/layui.css">
</head>
<body class="layui-layout-body">
	<div class="layui-layout layui-layout-admin">
		<div class="layui-header layui-bg-black">
			<div class="layui-logo">企业管理系统 pc版</div>
			<!-- 头部区域（可配合layui已有的水平导航） -->
			<ul class="layui-nav layui-layout-right">
				<li class="layui-nav-item"><a href="javascript:;">
					</li>
				<li class="layui-nav-item"><a id="quit" href="javascript:;">退了</a></li>
			</ul>
		</div>

		<div class="layui-side ">
			<div class="layui-side-scroll layui-bg-black">
				<!-- 左侧导航区域（可配合layui已有的垂直导航） -->
				<ul class="layui-nav layui-nav-tree" lay-filter="test">
					<li class="layui-nav-item"><a id="userMana"
						href="javascript:;">用户信息管理 </a></li>
					<li class="layui-nav-item"><a id="kMana"
						href="javascript:;">打卡管理 </a></li>
					<li class="layui-nav-item"><a id="cv_inforMana"
						href="javascript:;">考勤信息</a></li>
					<li class="layui-nav-item"><a id="salaryManager"
						href="javascript:;">薪资信息</a></li>
				</ul>
			</div>
		</div>

		<div class="layui-body">
			<!-- 内容主体区域 -->
			<div id="main" style="padding: 15px;">内容主体区域</div>
		</div>

		<div class="layui-footer">
			<!-- 底部固定区域 -->
			© layui.com - 底部固定区域
		</div>
	</div>
	<script src="js/layui.js"></script>
	<script>
		layui.use([ 'jquery', 'element' ], function() {
			var $ = layui.$;
			var element = layui.element;

			//用户管理
			$("#userMana").click(function() {
				$("#main").load("tbUser/index", {page:"后台"});
			});
			
			//打卡管理
			$("#kMana").click(function() {
				$("#main").load("tbWork/index", {page:"后台"});
			});

			//考勤信息
			$("#cv_inforMana").click(function() {
				$("#main").load("tbWorkCount/index", {page:"后台"});
			});
			//薪资管理
			$("#salaryManager").click(function() {
				$("#main").load("tbSalary/index", {page:"后台"});
			});
			
			//退出
			$("#quit").click(function(){
				var url="/job/quit.ajax";
				$.post(url,{},function(data){
					window.location.href="/job/";
				},'json');
			})
		})	
	</script>
</body>
</html>    