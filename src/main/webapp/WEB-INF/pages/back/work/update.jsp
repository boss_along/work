<%@ page contentType="text/html; charset=utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../../css/layui.css" media="all">
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<div class="layui-container">
    <form class="layui-form">
        <div class="layui-form-item">
            <label class="layui-form-label">用户名</label>
            <div class="layui-input-block">
                    <input type="text" name="userId" value="${work.userId}"
                           class="layui-input" readonly>
                <input type="hidden" name="id" value="${work.id}">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">打卡时间</label>
            <div class="layui-input-inline">
                <input type="text" value="${work.pattenCardTime}" name="cardTime" class="layui-input" id="test4" placeholder="yyyy-MM-dd HH:mm:ss">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">打卡类型</label>
            <div class="layui-input-block">
                <select id="type" name="type" value="${work.type}" lay-verify="required" lay-filter="test">
                    <option value="上班卡">上班卡</option>
                    <option value="下班卡">下班卡</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">打卡状态</label>
            <div class="layui-input-block">
                <select id="workStatus" name="workStatus" value="${work.workStatus}" lay-verify="required" lay-filter="test">
                    <option value="正常">正常</option>
                    <option value="迟到">迟到</option>
                    <option value="早退">早退</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">是否迟到</label>
            <div class="layui-input-block">
                <input type="text" name="isArriveLater" lay-verify="required" value="${work.isArriveLater}"
                       class="layui-input" readonly>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">是否早退 </label>
            <div class="layui-input-block">
                <input type="text" name="isLeaveEarly" lay-verify="required" value="${work.isLeaveEarly}"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="*">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
</div>
<script src="../../js/layui.js"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use([ 'form','jquery','layer','laydate' ],function() {
        var form = layui.form;
        var $=layui.$;
        var layer = layui.layer;
        var laydate = layui.laydate;

        //时间选择器
        laydate.render({
            elem: '#test4'
            ,type: 'datetime'
        });


        $("#type").val('${work.type}').trigger("change");
        $("#workStatus").val('${work.workStatus}').trigger("change");

        //监听提交
        form.on('submit(*)', function(data) {
            $.ajax({
                url:"/tbWork/update",
                data:data.field,
                type:'post',
                success:function(data){
                    if(data.success){
                        var index = parent.layer.getFrameIndex(window.name);
                        parent.layer.close(index);
                        parent.reloadTable();
                    }
                }
            })
            return false;
        });

    });
</script>

</body>
</html>