<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../../css/layui.css" media="all">
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<div class="layui-container">
    <form class="layui-form">
        <div class="layui-form-item">
            <label class="layui-form-label">用户名</label>
            <div class="layui-input-block">
                <select id="username" name="userId" lay-verify="required" lay-filter="test">
                    <c:forEach var="ele"   items="${list}">

                        <option value="${ele.id}">${ele.username}</option>

                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">打卡时间</label>
            <div class="layui-input-inline">
                <input type="text" name="cardTime" class="layui-input" id="test4" placeholder="yyyy-MM-dd HH:mm:ss">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">打卡类型</label>
            <div class="layui-input-block">
                <select name="type" lay-verify="required" lay-filter="test">
                    <option value="上班卡">上班卡</option>
                    <option value="下班卡">下班卡</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">打卡状态</label>
            <div class="layui-input-block">
                <input type="workStatus" name="phone" lay-verify="required" value="打卡"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="*">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
</div>
<script src="../../js/layui.js"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use([ 'form','jquery','layer','laydate' ],function() {
        var form = layui.form;
        var $=layui.$;
        var layer = layui.layer;
        var laydate = layui.laydate;

        //时间选择器
        laydate.render({
            elem: '#test4'
            ,type: 'datetime'
        });


        //监听提交
        form.on('submit(*)', function(data) {
            $.ajax({
                url:"/tbWork/add",
                data:data.field,
                type:'post',
                success:function(data){
                    if(data.success){
                        var index = parent.layer.getFrameIndex(window.name);
                        parent.layer.close(index);
                        parent.reloadTable();
                    }
                }
            })
            return false;
        });

    });
</script>

</body>
</html>