<%@ page contentType="text/html; charset=utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1">
</head>
<body>
<div>
    <div class="layui-btn-group demoTable">
        <div>
            <div class="layui-btn-group demoTable">
                <button class="layui-btn" id="add">新增</button>
            </div>
        </div>
    </div>
</div>

<table class="layui-hide" id="user_table" lay-data="{id: 'idTest'}" lay-filter="demo"></table>
    <script type="text/html" id="barDemo">
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<script>
    var tableIns;
    layui.use([ 'jquery','table','element', 'layer' ], function() {
        var $ = layui.$;
        var table = layui.table;
        var layer = layui.layer;
        var id = ${sessionScope.loginUser.id}
        tableIns = table.render({
            elem: '#user_table',
            url:'tbWorkCount/list',
            cols: [[{field:'user_id', width:220, title: '用户ID ', sort: false},
                {field:'year', width:220, title: '考勤年份 ', sort: false},
                {field:'month', width:250, title: '考勤月份', sort: false},
                {field:'workDay', width:250, title: '出勤天数', sort: false},
                {field:'restDay', width:180, title: '请假天数', sort: false},
                {field:'arriveLateDay', width:250, title: '迟到天数', sort: false},
                {field:'leaveEarly', width:250, title: '早退天数', sort: false},
                {field:'status', width:180, title: '状态', sort: false},
                {fixed: 'right', width:200, align:'center', toolbar: '#barDemo'}]],
            page: true,//分页
            // skin: 'line',
            size: 'xs',//表格尺寸
            even: true,//隔行变色
            limit:10,//初始每页显示条数
            limits:[10,20,30]//分页下拉
        });

        //删除操作
        table.on('tool(demo)',function(obj){
            var data = obj.data;
            var pk = data.id;
            if(obj.event === 'edit'){
                var index = layer.open({
                    type: 2,
                    title :'用户修改',
                    area: ['30%', '60%'],
                    content: 'tbWorkCount/toupdate/'+pk //这里content是一个普通的String
                });
            }else if(obj.event === 'del'){
                var url = 'tbWorkCount/del';
                layer.confirm('确定要删除?', function(index){
                    $.post(url,{id:pk},function(data){
                        if(data.success){
                            layer.msg('删除成功', {icon: 6});
                            reloadTable();
                        }
                    },'json');
                    layer.close(index);
                },function(){});

            }
        });


        //新增
        $("#add").click(function(){
            var url = "tbWorkCount/toadd";
            var index = layer.open({
                type: 2,
                title :'新增',
                area: ['30%', '60%'],
                content: url //这里content是一个普通的String
            });
        })
    });

    window.reloadTable = function (){
        tableIns.reload({
            where: { //设定异步数据接口的额外参数，任意设
                aaaaaa: 'xxx'
                ,bbb: 'yyy'
                //…
            }
            ,page: {
                curr: 1 //重新从第 1 页开始
            }
        });
    }
</script>
</body>
</html>